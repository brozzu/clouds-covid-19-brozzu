// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "",
    authDomain: "covid19-brozzu.firebaseapp.com",
    databaseURL: "https://covid19-brozzu.firebaseio.com",
    projectId: "covid19-brozzu",
    storageBucket: "covid19-brozzu.appspot.com",
    messagingSenderId: "659582665884",
    appId: "1:659582665884:web:17579dc68896cd6519d959",
    measurementId: "G-MJKX51BFEZ"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
