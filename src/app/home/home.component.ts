import { Component, OnInit } from '@angular/core';
import { GetDataService } from '../get-data.service';
import { ChartDataSets, ChartType, ChartOptions } from 'chart.js';
import { Color, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
import { LoginService } from '../login.service';
import { News } from '../news.model';
import { NewsService } from '../news.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public pieChartOptions: ChartOptions = {
    responsive: true,
  };
  public pieChartLabels: Label[] = ['Deaths', 'Recoveries', 'Active Cases'];
  public pieChartData: number[] = [];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  public pieChartColors = [
    {
      backgroundColor: ['rgba(255,0,0,0.3)', 'rgba(0,255,0,0.3)', 'rgba(0,0,255,0.3)'],
    },
  ];

  public weeklyChartOptions: ChartOptions = {
    responsive: true,
  };
  // Days
  public weeklyChartLabels: Label[] = [];
  public weeklyChartType: ChartType = 'bar';
  public weeklyChartLegend = true;

  // Series
  public weeklyChartData: ChartDataSets[] = [];

  // Cumulative graph

  public totalChartData: ChartDataSets[] = [];
  public totalChartLabels: Label[] = [];
  public totalChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        },
        {
          id: 'y-axis-1',
          position: 'right',
          gridLines: {
            color: 'rgba(255,0,0,0.3)',
          },
          ticks: {
            fontColor: 'red',
          }
        }
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis-0',
          value: 'March',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    },
  };
  public totalChartColors: Color[] = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // red
      backgroundColor: 'rgba(255,0,0,0.3)',
      borderColor: 'red',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public totalChartLegend = true;
  public totalChartType: ChartType = 'line';

  public covid_data = {
    Global: [],
    Countries: [],
  };
  cases = [];
  classes = [];
  test = [0,1,2];
  classes_values = {
    NewConfirmed: "New confirmed cases",
    NewDeaths: "New deaths",
    NewRecovered: "New Recoveries",
    TotalConfirmed: "Total Confirmed Cases",
    TotalDeaths: "Total Deaths",
    TotalRecovered: "Total Recoveries"

  };

  public news;
  
  constructor(public dataService: GetDataService, public loginService: LoginService, public newsService: NewsService, public router: Router) { 
    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
  }

  ngOnInit(): void {
    this.covid_data['Global'] = [];

    this.dataService.getSummaryData().then(response => {
      this.covid_data['Global'] = response['Global'];
      this.pieChartData = [ 
        this.covid_data['Global']['TotalDeaths'], 
        this.covid_data['Global']['TotalRecovered'], 
        this.covid_data['Global']['TotalConfirmed']- this.covid_data['Global']['TotalDeaths']- this.covid_data['Global']['TotalRecovered'],
      ];
      console.log("data obtained:")
      console.log(this.covid_data)
    });
  
    this.dataService.getGlobalWeeklyDataCumulative().then(response => {
      var histogram_data = [];
      this.weeklyChartData = [];
      var labels = ['recovered', 'deaths', 'cases'];
      for (var i = 0; i<labels.length; i++){
        var curr_series = [];
        length = response[labels[i]].length;
        for (var j = 7; j>0; j--){
          var curr_value = parseInt(response[labels[i]][length-j])-parseInt(response[labels[i]][length-j-1]);
          curr_series.push(curr_value);
        }
        histogram_data.push({
          data: curr_series,
          label: labels[i]
        });
      }

      var dates = [];
      for (var i = 7; i>0; i--){
        dates.push(response['dates'][length-i]);
      }

      this.weeklyChartData = histogram_data;
      this.weeklyChartLabels = dates;

      console.log(this.weeklyChartData);
      console.log(this.weeklyChartLabels);
       
    });

    this.dataService.getGlobalDataCumulative().then(response => {
      console.log("weekly data: ");
      console.log(response);
      this.totalChartData = [];
      var chart_data = [];
      var labels = ['deaths', 'cases', 'recovered'];
      for (var i = 0; i<labels.length; i++){
        chart_data.push({
          data: response[labels[i]],
          label: labels[i]
        });
      }
      this.totalChartData = chart_data;
      this.totalChartLabels = response['dates']
    });

    this.newsService.getNews("worldwide").subscribe(response => {
      this.news = [];
      this.news = response;
      console.log("Obtained news: ")
      console.log(this.news);
    })
  }
}
