import { Injectable } from '@angular/core';
import firebase from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { User } from './user.model';
import { Router } from '@angular/router';
import { trigger } from '@angular/animations';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  user: Observable<firebase.User>;
  
  constructor(private firebaseAuth: AngularFireAuth, private router: Router) {
    this.user = firebaseAuth.authState;
  }

  async signInWithGoogle(){
    this.firebaseAuth
      .signInWithPopup(new firebase.auth.GoogleAuthProvider())
      .then(value => {
        console.log('Success!', value);
      })
      .catch(err => {
        console.log('Something went wrong:',err.message);
      });
  } 

  getUser() {
    if (this.user == null && this.userSignedIn()) {
      this.user = JSON.parse(localStorage.getItem("user"));
    }
    return this.user;
  }

  userSignedIn(): boolean {
    return JSON.parse(localStorage.getItem("user")) != null;
  }

  userEligible(): boolean {
    return localStorage.getItem("eligible") == "true";
  }

  signOut() {
    this.firebaseAuth.signOut();
    this.router.navigate(["/"]);
  }

}
