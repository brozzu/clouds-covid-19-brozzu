import { Injectable } from '@angular/core';
import { GlobalStatsType } from './global-stats-type'
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Output, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class GetDataService {

  private api_url = 'https://api.covid19api.com/summary';
  private api_weekly_url = 'https://corona.lmao.ninja/v2/historical/all?lastdays=8';
  private api_cumulative_url = 'https://corona.lmao.ninja/v2/historical/all';
  private api_country_details_url = 'https://api.covid19api.com/dayone/country';
  classes: string[] = [];
  cases = [];

  global_stats;
  stats = {
    Global: [],
    Countries: [],
  };

  constructor(private firestore: AngularFirestore, private http: HttpClient) {
  }


  getSummaryData() {
    return new Promise(resolve => {
      const date_collection = this.firestore.collection('statistics').doc('global_date');
      const stats_collection = this.firestore.collection('statistics').doc('stats');
      var today_split = new Date().toISOString().split('T');
      var today = today_split[0];
      date_collection.get().subscribe(query_date => {
        if (query_date.exists) {
          // If the element exists => subscribe!
          date_collection.valueChanges().subscribe(response_date => {
            console.log(response_date);
            var date_str = response_date['date'];
            var date_split = date_str.split('T');
            var date = date_split[0];
            if (!this.checkDate(date, today)) {
              console.log("Timestamp is different updating data!");
              this.http.get(this.api_url).subscribe(response_http => {
                stats_collection.set(response_http, { merge: true });
                date_collection.set({date: response_http['Date']});
                resolve(response_http);
              });
            }
            else {
              stats_collection.valueChanges().subscribe( response_firestore =>{
                resolve(response_firestore);
              });              
            }
          });
        }
        else {
          console.log("No data, try and get it!")
          this.http.get(this.api_url).subscribe(response => {
            stats_collection.set(response, { merge: true });
            date_collection.set({date: response['Date']});
            resolve(response);
          });
        }
      });
    });
  }

  getGlobalWeeklyDataCumulative() {
    return new Promise(resolve => {
      const date_collection = this.firestore.collection('statistics').doc('date');
      const stats_collection = this.firestore.collection('statistics').doc('weekly_data_cumulative');
      var today_split = new Date().toISOString().split('T');
      var today = today_split[0];
      date_collection.get().subscribe(response => {
        if (response.exists) {
          date_collection.valueChanges().subscribe(resp => {
            // If updated get from DB, else...
            var date_str = resp['date'];
            var date_split = date_str.split('T');
            var date = date_split[0];
            if (!this.checkDate(date, today)) {
              console.log("Timestamp is different updating data!");
              this.http.get(this.api_weekly_url).subscribe(response => {
                var manipulated_data = this.updateGlobalWeeklyData(response);
                this.firestore.collection("statistics").doc('weekly_data_cumulative').set(manipulated_data, { merge: true });
                resolve(manipulated_data);
              });
            }
            else {
              resolve(resp);
            }
          });
        }
        else {
          console.log("No data available, fill DB!")
          this.http.get(this.api_weekly_url).subscribe(response => {
            var manipulated_data = this.updateGlobalWeeklyData(response);
            this.firestore.collection("statistics").doc('weekly_data_cumulative').set(manipulated_data, { merge: true });
            resolve(manipulated_data);
          });
        }
      });
    });
  }

  getGlobalDataCumulative() {
    return new Promise(resolve => {
      const date_collection = this.firestore.collection('statistics').doc('date');
      const stats_collection = this.firestore.collection('statistics').doc('all_data_cumulative');
      var today_split = new Date().toISOString().split('T');
      var today = today_split[0];
      date_collection.get().subscribe(response => {
        if (response.exists) {
          date_collection.valueChanges().subscribe(resp => {
            // If updated get from DB, else...
            var date_str = resp['date'];
            var date_split = date_str.split('T');
            var date = date_split[0];
            if (!this.checkDate(date, today)) {
              console.log("Timestamp is different updating data!");
              this.http.get(this.api_cumulative_url).subscribe(response => {
                var manipulated_data = this.updateGlobalCumulativeData(response);
                this.firestore.collection("statistics").doc('all_data_cumulative').set(manipulated_data, { merge: true });
                resolve(manipulated_data);
              });
            }
            else {
              resolve(resp);
            }
          });
        }
        else {
          console.log("No data available, fill DB!")
          this.http.get(this.api_cumulative_url).subscribe(response => {
            var manipulated_data = this.updateGlobalCumulativeData(response);
            this.firestore.collection("statistics").doc('all_data_cumulative').set(manipulated_data, { merge: true });
            resolve(manipulated_data);
          });
        }
      });
    });
  }

  getCountryDetails(country) {
    return new Promise(resolve => {
      const date_collection = this.firestore.collection('statistics').doc(country + '_date');
      const stats_collection = this.firestore.collection("country_details").doc(country);
      var today_split = new Date().toISOString().split('T');
      var today = today_split[0];
      date_collection.get().subscribe(query_date => {
        if (query_date.exists) {
          date_collection.valueChanges().subscribe(response_date => {
            // If updated get from DB, else...
            var date_str = response_date['date'];
            var date_split = date_str.split('T');
            var date = date_split[0];
            if (!this.checkDate(date, today)) {
              console.log("Timestamp is different updating data!");
              this.http.get(this.api_country_details_url + "/" + country).subscribe(response => {
                var manipulated_data = this.updateCountryData(response);
                var latest_date = manipulated_data['dates'][manipulated_data['dates'].length-1];
                stats_collection.set(manipulated_data, { merge: true });
                date_collection.set({date: latest_date});
                resolve(manipulated_data);
              });
            }
            else{
              console.log("Data is updated, show firestore")
              stats_collection.valueChanges().subscribe(response_firestore =>{
                resolve(response_firestore);
              });              
            }
          });
        }
        else {
          console.log("No data available, fill DB!")
          this.http.get(this.api_country_details_url + "/" + country).subscribe(response => {
            var manipulated_data = this.updateCountryData(response);
            var latest_date = manipulated_data['dates'][manipulated_data['dates'].length-1];
            this.firestore.collection("country_details").doc(country).set(manipulated_data, { merge: true });
            date_collection.set({date: latest_date});
            resolve(manipulated_data);
          });
        }
      });
    });
    
  }

  checkDate(date1, date2) {
    console.log(date1)
    console.log(date2)
    return (date1 == date2) ? true : false
  }

  updateData() {
    this.http.get(this.api_url).subscribe(response => {
      this.firestore.collection("statistics").doc('stats').set(response, { merge: true });
    });
  }

  updateCountryData(response) {
      var data_list = Object.keys(response).map(function (k) { return response[k] });
      var data_dict = {
        confirmed: [],
        deaths: [],
        recovered: [],
        dates: []
      };
      data_list.sort(function (a, b) {
        return b.date - a.date;
      })
      console.log(data_list)
      for (var i = 0; i < data_list.length; i++) {
        data_dict['confirmed'].push(data_list[i].Confirmed);
        data_dict['deaths'].push(data_list[i].Deaths);
        data_dict['recovered'].push(data_list[i].Recovered);
        data_dict['dates'].push(data_list[i].Date);
      }
      return data_dict;
  }

  updateGlobalWeeklyData(response) {
    // Perform some data manipulation and store
    var weekly_data_cumulative = {
      deaths: [],
      cases: [],
      recovered: [],
      dates: []
    }

    for (const [key, value] of Object.entries(response['cases'])) {
      weekly_data_cumulative['dates'].push(key);
    }
    console.log(weekly_data_cumulative);
    for (const [key, value] of Object.entries(response)) {
      for (const [key2, value2] of Object.entries(value)) {
        weekly_data_cumulative[key].push(value2);
      }
    }

    return weekly_data_cumulative;

  }

  updateGlobalCumulativeData(response) {

    var all_data_cumulative = {
      deaths: [],
      cases: [],
      recovered: [],
      dates: []
    }
    for (const [key, value] of Object.entries(response['cases'])) {
      all_data_cumulative['dates'].push(key);
    }
    console.log(all_data_cumulative);
    for (const [key, value] of Object.entries(response)) {
      for (const [key2, value2] of Object.entries(value)) {
        all_data_cumulative[key].push(value2);
      }
    }
    return all_data_cumulative;
  }
}


