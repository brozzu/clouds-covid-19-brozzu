import { Component, OnInit } from '@angular/core';
import { GetDataService } from '../get-data.service';
import { ActivatedRoute } from '@angular/router';
import { ChartDataSets, ChartType, ChartOptions } from 'chart.js';
import { Color, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
import { LoginService } from '../login.service';
import { NewsService } from '../news.service';

@Component({
  selector: 'app-country-details',
  templateUrl: './country-details.component.html',
  styleUrls: ['./country-details.component.css']
})
export class CountryDetailsComponent implements OnInit {

  public country: String;
  public country_summary = {
    Country: "",
    CountryCode: "",
    Date: "",
    ID: "",
    NewConfirmed: 0,
    NewDeaths: 0,
    NewRecovered: 0 
  };
  public country_details;
  public pieChartOptions: ChartOptions = {
    responsive: true,
  };
  public pieChartLabels: Label[] = ['Deaths', 'Recoveries', 'Active Cases'];
  public pieChartData: number[] = [];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  public pieChartColors = [
    {
      backgroundColor: ['rgba(255,0,0,0.3)', 'rgba(0,255,0,0.3)', 'rgba(0,0,255,0.3)'],
    },
  ];

  public weeklyChartOptions: ChartOptions = {
    responsive: true,
  };
  // Days
  public weeklyChartLabels: Label[] = [];
  public weeklyChartType: ChartType = 'bar';
  public weeklyChartLegend = true;

  // Series
  public weeklyChartData: ChartDataSets[] = [];

  // Cumulative graph

  public totalChartData: ChartDataSets[] = [];
  public totalChartLabels: Label[] = [];
  public totalChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        },
        {
          id: 'y-axis-1',
          position: 'right',
          gridLines: {
            color: 'rgba(255,0,0,0.3)',
          },
          ticks: {
            fontColor: 'red',
          }
        }
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis-0',
          value: 'March',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    },
  };
  public totalChartColors: Color[] = [
    { // grey
      backgroundColor: 'rgba(0,255,0,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(0,0,255,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // red
      backgroundColor: 'rgba(255,0,0,0.3)',
      borderColor: 'red',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public totalChartLegend = true;
  public totalChartType: ChartType = 'line';

  classes_values = {
    NewConfirmed: "New confirmed cases",
    NewDeaths: "New deaths",
    NewRecovered: "New Recoveries",
    TotalConfirmed: "Total Confirmed Cases",
    TotalDeaths: "Total Deaths",
    TotalRecovered: "Total Recoveries"
  };

  public news;

  constructor(private activatedRoute: ActivatedRoute, public dataService: GetDataService, public loginService: LoginService, public newsService: NewsService) { }

  ngOnInit(): void {
    // Get country name from URL
    this.country_summary['Country'] = "";

    this.activatedRoute.params.subscribe((params) => {
      this.country = params['country'];

      this.dataService.getSummaryData().then( response => {
        response['Countries'].forEach(element => {
          if (element.Slug == this.country){
            this.country_summary = element;
            this.pieChartData = [ 
              this.country_summary['TotalDeaths'], 
              this.country_summary['TotalRecovered'], 
              this.country_summary['TotalConfirmed']- this.country_summary['TotalDeaths']- this.country_summary['TotalRecovered'],
            ];
          }
        });
      });


      this.dataService.getCountryDetails(this.country).then(response => {
        this.country_details = response;
        console.log(this.country_details);
        this.totalChartData = [];
        var chart_data = [];
        var labels = ['recovered', 'deaths', 'confirmed'];
        for (var i = 0; i<labels.length; i++){
          chart_data.push({
            data: this.country_details[labels[i]],
            label: labels[i]
          });
        }
        this.totalChartData = chart_data;
        this.totalChartLabels = response['dates'];
        
        // Do some operations to get the weekly new cases/confirmed/deaths

        var histogram_data = [];
        this.weeklyChartData = [];
        for (var i = 0; i<labels.length; i++){
          var curr_series = [];
          length = this.country_details[labels[i]].length;
          for (var j = 8; j>0; j--){
            var curr_value = parseInt(this.country_details[labels[i]][length-j])-parseInt(this.country_details[labels[i]][length-j-1]);
            curr_series.push(curr_value);
          }
          histogram_data.push({
            data: curr_series,
            label: labels[i]
          });
        }

        var dates = [];
        for (var i = 7; i>0; i--){
          dates.push(this.country_details['dates'][length-i]);
        }

        this.weeklyChartData = histogram_data;
        this.weeklyChartLabels = dates;

        console.log(this.weeklyChartData);
        console.log(this.weeklyChartLabels);
        

      });
      
      this.newsService.getNews(this.country).subscribe(response => {
        this.news = [];
        this.news = response;
        console.log("Obtained news: ")
        console.log(this.news);
      });

    });

    


  }

}
