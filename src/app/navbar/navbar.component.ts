import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { GetDataService } from '../get-data.service';
import { LoginService } from '../login.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  public eligible = false;

  constructor(public dataService: GetDataService, public loginService: LoginService, private firestore: AngularFirestore) { }

  ngOnInit(): void {
    this.loginService.user.subscribe(response => {
      if (response) {
        this.firestore.collection("eligible_users", ref => ref.where('uid', '==', response.uid)).valueChanges().subscribe(response_eligible => {
          if (response_eligible.length > 0) {
            this.eligible = true;
          }
        }
        );
      }
    });
  }

}
