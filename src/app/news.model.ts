import { User } from "./user.model";

export interface News {
    title: string;
    subtitle: string;
    text: string;
    author: string;
    country: string;
}