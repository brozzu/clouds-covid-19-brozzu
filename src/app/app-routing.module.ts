import { NgModule } from '@angular/core';
import { HomeComponent } from './home/home.component'
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { CountryListComponent } from './country-list/country-list.component';
import { CountryDetailsComponent } from './country-details/country-details.component';
import { NewsUploadComponent } from './news-upload/news-upload.component';
import { SecurepagesGuard } from './securepages.guard';

const routes: Routes = [
  { path: "", component: HomeComponent},
  { path: "country_list", component: CountryListComponent },
  { path: "country_details/:country", component: CountryDetailsComponent},
  { path: "upload", component: NewsUploadComponent, canActivate: [SecurepagesGuard]},
  { path: "**", component: HomeComponent},
];

@NgModule({
  imports: [CommonModule, RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
