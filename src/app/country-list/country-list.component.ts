import { Component, OnInit } from '@angular/core';
import { GetDataService } from '../get-data.service';
import { LoginService } from '../login.service';

@Component({
  selector: 'app-country-list',
  templateUrl: './country-list.component.html',
  styleUrls: ['./country-list.component.css']
})
export class CountryListComponent implements OnInit {
  public covid_data = {
    Global: [],
    Countries: [],
  };

  classes_values = {
    NewConfirmed: "New confirmed cases",
    NewDeaths: "New deaths",
    NewRecovered: "New Recoveries",
    TotalConfirmed: "Total Confirmed Cases",
    TotalDeaths: "Total Deaths",
    TotalRecovered: "Total Recoveries"
  };


  constructor(public dataService: GetDataService, public loginService: LoginService) { }

  ngOnInit(): void {
    this.covid_data['Countries'] = [];

    this.dataService.getSummaryData().then(response => {
      this.covid_data['Countries'] = response['Countries'];
      console.log("data obtained:")
      console.log(this.covid_data)
    });
  }

}
