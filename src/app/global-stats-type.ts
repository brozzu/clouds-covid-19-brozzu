export interface GlobalStatsType {
    NewConfirmed: number,
    TotalConfirmed,
    NewDeaths,
    TotalDeaths,
    NewRecovered,
    TotalRecovered
}
