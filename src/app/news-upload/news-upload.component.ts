import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login.service';
import { News } from '../news.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { NewsService } from '../news.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-news-upload',
  templateUrl: './news-upload.component.html',
  styleUrls: ['./news-upload.component.css']
})
export class NewsUploadComponent implements OnInit {

  constructor(public loginService: LoginService, public newsService: NewsService, private firestore: AngularFirestore, private router: Router) { }

  public myNews: News;

  public title: string;
  public subtitle: string;
  public text: string;
  public country; String;

  ngOnInit(): void {
    this.myNews = {
      title: "Title",
      subtitle: "Subtitle",
      text: "Text",
      author: "",
      country: "worldwide",
    }
  }

  uploadNews(){
    console.log(this.myNews);
    this.loginService.user.subscribe( response => {
      this.firestore.collection("eligible_users", ref => ref.where('uid', '==', response.uid)).valueChanges().subscribe( response_eligible =>
        {
          console.log(response)
          console.log(response_eligible)
         if (response_eligible.length > 0){
          this.myNews.author = response.displayName;
          this.newsService.uploadNews(this.myNews);
          if (this.myNews.country != "worldwide"){
            this.router.navigate(["/country_details/" + this.myNews.country]);
          }
          else{
            this.router.navigate(["/"]);
          }
         }
         else{
          window.alert("Error you are not eligible to upload news");
          this.router.navigate(["/"]);
         }
         
        }
      );
    });
    
  }

}
