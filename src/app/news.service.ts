import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { News } from './news.model';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  constructor(private firestore: AngularFirestore, private http: HttpClient) { 
    
  }

  getNews(country){
    console.log("Query DB for news from: " + country)
    const news_collection = this.firestore.collection('news', ref => ref.where('country', '==', country));
    return news_collection.valueChanges();
  }

  uploadNews(news){
    this.firestore.collection("news").add(news);
  }
}
