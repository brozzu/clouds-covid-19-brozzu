import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class SecurepagesGuard implements CanActivate {
  constructor(private router: Router, private loginService: LoginService, private firestore: AngularFirestore) {

  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return new Promise(resolve => {
      this.loginService.user.subscribe(response => {
        if (response) {
          this.firestore.collection("eligible_users", ref => ref.where('uid', '==', response.uid)).valueChanges().subscribe(response_eligible => {
            if (response_eligible.length > 0) {
              resolve(true)
            }
            else {
              resolve(false)
            }
          });
        }
        else {
          resolve(false);
        }
      });
    });
  }
}
