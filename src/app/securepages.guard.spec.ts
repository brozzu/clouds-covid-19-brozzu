import { TestBed } from '@angular/core/testing';

import { SecurepagesGuard } from './securepages.guard';

describe('SecurepagesGuard', () => {
  let guard: SecurepagesGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(SecurepagesGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
